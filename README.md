# [acme-http-01-test](https://git.rootprojects.org/root/acme-http-01-test.js.git) | a [Root](https://rootprojects.org) project

An ACME https-01 test harness for Let's Encrypt integrations.

This was specificially designed for [ACME.js](https://git.coolaj86.com/coolaj86/acme-v2.js) and [Greenlock.js](https://git.coolaj86.com/coolaj86/greenlock-express.js), but will be generically useful to any ACME module.

Passing the tests is very easy. There are just three functions to implement:

- `set()` - set a TXT record in a zone (i.e. `_acme-challenge.foo` in `example.com`)
- `get()` - confirm that the record was set
- `remove()` - clean up after the ACME challenge passes

The http-01 tests account for single-domain certificates (`example.com`).
If you need multiple domain certs (SAN / AltName),
wildcards (`*.example.com`), or valid private / localhost certificates,
you'll need [acme-dns-01-test.js](https://git.rootprojects.org/root/acme-http-01-test.js.git) instead.

**Node v6 Support**: Please build community plugins using node v6 / vanillajs
to ensure that all acme.js and greenlock.js users are fully supported.

## Install

```bash
npm install --save-dev acme-http-01-test@3.x
```

## Usage

```js
var tester = require('acme-http-01-test');

//var challenger = require('acme-http-01-cli').create({});
var challenger = require('./YOUR-CHALLENGE-STRATEGY').create({
  YOUR_TOKEN_OPTION: 'SOME_API_KEY'
});

// The dry-run tests can pass on, literally, 'example.com'
// but the integration tests require that you have control over the domain
var record = 'foo.example.com';

tester.testRecord('http-01', record, challenger).then(function() {
  console.info('PASS');
});
```

**Note**: If the service you are testing only handles multiple records
within a single zone, you should use `testZone` instead:

```js
var zone = 'example.co.uk';

tester.testZone('http-01', zone, challenger).then(function() {
  console.info('PASS');
});
```

## Reference Implementations

These are plugins that use the v2.7+ (v3) API, and pass this test harness,
which you should use as a model for any plugins that you create.

- http-01
  - [`acme-http-01-cli`](https://git.rootprojects.org/root/acme-http-01-cli.js)
  - [`acme-http-01-fs`](https://git.rootprojects.org/root/acme-http-01-fs.js)
- dns-01
  - [`acme-dns-01-cli`](https://git.rootprojects.org/root/acme-dns-01-cli.js)
  - [`acme-dns-01-digitalocean`](https://git.rootprojects.org/root/acme-dns-01-digitalocean.js)

You can find other implementations by searching npm for [acme-http-01-](https://www.npmjs.com/search?q=acme-http-01-)
and [acme-dns-01-](https://www.npmjs.com/search?q=acme-dns-01-).

If you are building a plugin, please let us know.
We may like to co-author and help maintain and promote your module.

## Example

See `example.js` (it works).

## Starter Template

Here's what you could start with.

```js
var tester = require('acme-http-01-test');

// The dry-run tests can pass on, literally, 'example.com'
// but the integration tests require that you have control over the domain
var record = 'example.com';

tester
  .testRecord('http-01', record, {
    // Should make the token url return the key authorization
    // i.e. GET http://example.com/.well-known/acme-challenge/xxxx => xxxx.yyyy
    set: function(opts) {
      console.log('set opts:', opts);
      throw new Error('set not implemented');
    },

    // Should remove the previously set token file (just the one)
    remove: function(opts) {
      console.log('remove opts:', opts);
      throw new Error('remove not implemented');
    },

    // Should get the token file via the hosting service API
    get: function(opts) {
      console.log('get opts:', opts);
      throw new Error('get not implemented');
    }
  })
  .then(function() {
    console.info('PASS');
  });
```

## http-01 vs dns-01

For `type` http-01:

    // `altname` is the name of the domain
    // `token` is the name of the file ( .well-known/acme-challenge/`token` )
    // `keyAuthorization` is the contents of the file

For `type` dns-01:

    // `dnsHost` is the domain/subdomain/host
    // `dnsAuthorization` is the value of the TXT record

See [acme-dns-01-test.js](https://git.rootprojects.org/root/acme-http-01-test.js.git).

## Detailed Overview

Here's a quick pseudo stub-out of what a test-passing plugin object might look like:

```js
tester
  .testRecord('http-01', 'foo.example.com', {
    set: function(opts) {
      var ch = opts.challenge;
      // { type: 'http-01'
      // , identifier: { type: 'dns', value: 'foo.example.com' }
      // , token: 'xxxx'
      // , keyAuthorization: 'xxxx.yyyy' }

      return YourApi('POST', 'https://examplehost.com/api/sites/', {
        site: ch.identifier.value,
        filename: new URL(ch.url).pathname,
        contents: ch.keyAuthorization
      });
    },

    get: function(query) {
      var ch = query.challenge;
      // { type: 'http-01'
      // , identifier: { type: 'dns', value: 'foo.example.com' }
      // , token: 'xxxx'
      // , url: '...' }
      // Note: query.identifier.value is different for http-01 than for dns-01

      return YourApi(
        'GET',
        'https://examplehost.com/api/sites/' +
          ch.indentifier.value +
          '/' +
          new URL(ch.url).pathname
      ).then(function(secret) {
        // http-01
        return { keyAuthorization: secret };
      });
    },

    remove: function(opts) {
      var ch = opts.challenge;
      // same options as in `set()` (which are not the same as `get()`

      return YourApi(
        'DELETE',
        'https://examplehost.com/api/sites/' +
          ch.indentifier.value +
          '/' +
          new URL(ch.url).pathname
      );
    }
  })
  .then(function() {
    console.info('PASS');
  });
```

Where `YourApi` might look something like this:

```js
var YourApi = function createApi(config) {
  var request = require('@root/request');
  request = require('util').promisify(request);

  return function(method, url, body) {
    return request({
      method: method,
      url: url,
      json: body || true,
      headers: {
        Authorization: 'Bearer ' + config.apiToken
      }
    }).then(function(resp) {
      return resp.body;
    });
  };
};
```

### Two notes:

Note 1:

The `API.get()`, `API.set()`, and `API.remove()` is where you do your magic up to upload a file to the correct
location on an http serever or add the appropriate data to the database that handles such things.

Note 2:

You can't do wildcards with http-01 challenges.
